import React, { FunctionComponent, useEffect, useState } from 'react';
import { Button, Divider, List, Tag } from 'antd';
import { useTranslation } from 'react-i18next';
import { useParams, useHistory } from "react-router-dom";
import { TaskListProps } from '../../interfaces/atoms'

export const TaskList = ({ tasks, loading }: TaskListProps) => {
	const { t } = useTranslation();
  const { id } = useParams<{ id: string }>();
	const history = useHistory();
  
  const getTagColor = (type: string) => {
    switch (type) {
      case 'bug':
        return 'red'
      case 'refactor':
        return 'orange'
      case 'backlog':
        return 'blue'
      default:
        return 'gray'
    }
  }

  return (
    <>
     { tasks && 
      <List
        grid={{ gutter: 16, column: 1 }}
        dataSource={tasks}
        renderItem={(item:any) => (
          <List.Item>
            <List.Item.Meta
              avatar={
                <Tag color={getTagColor(item.type)}>{item.type.toUpperCase()}</Tag>
              }
              title={<a onClick={() => history.push(`/tasks/${item._id}`)}>#{item.number} - {item.name}</a>}
              description={`Estimado: ${item.estimated}`}
            />
            <Divider></Divider>
          </List.Item>
        )}
      />}
    </>
  );
}
