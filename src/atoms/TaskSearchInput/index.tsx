import React, { FunctionComponent, useEffect, useState } from 'react';
import { AutoComplete, Input, message } from 'antd';
import { useTranslation } from 'react-i18next';
import { useParams, useHistory } from "react-router-dom";
import { ITaskSearchInput } from '../../interfaces/atoms';
import { getTasksByNameReq } from '../../api/rest/queries/task';
const { TextArea, Search } = Input;

export const TaskSearchInput = ({ handleSelectTask, loading, searchTermOut }: ITaskSearchInput) => {
	const { t } = useTranslation();
  const { id } = useParams<{ id: string }>();
  const [taskOptions, setTaskOptions] = useState<any>([]);
  const [searchTerm, setSearchTerm] = useState<string | undefined>(searchTermOut)

  const handleSearchUser = (searchTerm: string) => {
    if(searchTerm.length < 3) return

    getTasksByNameReq(searchTerm).then(response => {
      const filteredData = response.data.map((item: any) => {
        return { value: item._id, label: item.name }
      })
      setTaskOptions(filteredData);
    }).catch(err => {
      console.error(err);
      message.error(t('projects.errorFetch'));
    })
  }

  const handleSelect = (item: any, option: any) => {
    handleSelectTask(option);
    setSearchTerm(option.label)
  }


  return (
    <AutoComplete
      options={taskOptions}
      onSelect={handleSelect}
      onSearch={handleSearchUser}
      value={searchTerm}
      onChange={(value) => setSearchTerm(value)}
      notFoundContent={t('manageTask.notFound')}
      disabled={loading}
    >
      <Search placeholder={t('manageTask.searchPlaceholder')} enterButton />
    </AutoComplete>
  );
}
