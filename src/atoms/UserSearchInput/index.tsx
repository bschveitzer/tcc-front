import React, { FunctionComponent, useEffect, useState } from 'react';
import { AutoComplete, Input, message } from 'antd';
import { useTranslation } from 'react-i18next';
import { useParams, useHistory } from "react-router-dom";
import { IUserSearchInput } from '../../interfaces/atoms';
import { getUsersByNameReq } from '../../api/rest/queries/user';
const { TextArea, Search } = Input;

export const UserSearchInput = ({ handleSelectUser, loading }: IUserSearchInput) => {
	const { t } = useTranslation();
  const { id } = useParams<{ id: string }>();
  const [userOptions, setUserOptions] = useState<any>([]);
  const [searchTerm, setSearchTerm] = useState<string>('')

  const handleSearchUser = (searchTerm: string) => {
    if(searchTerm.length < 3) return

    getUsersByNameReq(searchTerm).then(response => {
      const filteredData = response.data.map((item: any) => {
        return { value: item.id, label: item.fullName }
      })
      setUserOptions(filteredData);
    }).catch(err => {
      console.error(err);
      message.error(t('projects.errorFetch'));
    })
  }

  const handleSelect = (item: any) => {
    handleSelectUser(item);
    setSearchTerm('');
    setUserOptions([]);
  }


  return (
    <AutoComplete
      options={userOptions}
      onSelect={(item) => handleSelect(item)}
      onSearch={handleSearchUser}
      value={searchTerm}
      onChange={(value) => setSearchTerm(value)}
      notFoundContent={t('manageUser.notFound')}
      disabled={loading}
    >
      <Search size="large" placeholder={t('manageUser.searchPlaceholder')} enterButton />
    </AutoComplete>
  );
}
