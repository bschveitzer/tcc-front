import React, { FunctionComponent, useEffect, useState } from 'react';
import { Button, List, Avatar, } from 'antd';
import { useTranslation } from 'react-i18next';
import { useParams, useHistory } from "react-router-dom";
import { UserListProps } from '../../interfaces/atoms'

export const UserList = ({ handleRemove, users, loading }: UserListProps) => {
	const { t } = useTranslation();
  const { id } = useParams<{ id: string }>();

  const checkHandleRemove = (item: any) => {
    if(handleRemove) {
      return (<Button danger type="text" onClick={() => handleRemove(item)}>{ t('button.remove') }</Button>)
    }
    return null
  }

  return (
    <List
        itemLayout="horizontal"
        dataSource={users}
        loading={loading}
        renderItem={(item:any) => (
          <List.Item actions={[ checkHandleRemove(item) ]}>
            <List.Item.Meta
              avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
              title={<span>{item.fullName}</span>}
              description={item.login}
            />
          </List.Item>
        )}
      />
  );
}
