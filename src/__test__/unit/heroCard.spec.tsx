import React from 'react';
import { HeroCard } from '../../components/HeroCard/index';
import { fireEvent, render, RenderResult } from "@testing-library/react";

Object.defineProperty(window, 'matchMedia', {
  writable: true,
  value: jest.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(), // deprecated
    removeListener: jest.fn(), // deprecated
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    dispatchEvent: jest.fn(),
  })),
});

let props = {
  hero: {
    id: '12',
    name: 'Herói teste',
    thumbnail: 'https://images.tcdn.com.br/img/img_prod/681204/noticia_16036494475e64410be4a99.png',
    description: 'Descrição',
    series: [{name: ''}]
  },
  onClick: () => {}
}
let documentBody: RenderResult;

describe('<HeroCard />', () => {
  beforeEach(() => {
    documentBody = render(<HeroCard {...props}/>);
  });

  it('should render', () => {
    expect(documentBody.getByRole('img', { name: 'hero-img'})).toBeInTheDocument();
  });
});
