import React from 'react';
import { SearchInput } from '../../components/SearchInput/index';
import { fireEvent, render, RenderResult } from "@testing-library/react";

Object.defineProperty(window, 'matchMedia', {
  writable: true,
  value: jest.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(), // deprecated
    removeListener: jest.fn(), // deprecated
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    dispatchEvent: jest.fn(),
  })),
});

let request = () => {};
let props = {
  request,
  placeholder: 'Search'
}
let documentBody: RenderResult;

describe('<SearchInput />', () => {
  beforeEach(() => {
    documentBody = render(<SearchInput {...props}/>);
  });

  it('should render', () => {
    expect(documentBody.getByPlaceholderText('Search')).toBeInTheDocument();
  });

  it('should change value null to something', () => {
    const input = documentBody.getByPlaceholderText('Search');
    
    expect(input).toHaveProperty('value', '');
    
    fireEvent.change(input, { target: { value: 'React' } });
    expect(input).toHaveProperty('value', 'React');
  })

  it('should change value something to null', () => {
    const input = documentBody.getByPlaceholderText('Search');

    fireEvent.change(input, { target: { value: 'React' } });
    expect(input).toHaveProperty('value', 'React');

    fireEvent.change(input, { target: { value: '' } });
    expect(input).toHaveProperty('value', '');
  })

  it('should change placeholder', () => {
    documentBody.rerender(<SearchInput request={request} placeholder='Find repositories'/>)

    expect(documentBody.getByPlaceholderText('Find repositories')).toBeInTheDocument();
  })
});
