import React, { FunctionComponent, useState, useEffect } from 'react';
import { AppRouter } from '../../routes/Router';
import { Layout, Menu, message } from 'antd';
import { LogoNav } from '../../style/global';
import logoNav from '../../assets/brand/logo.svg';
import { useTranslation } from 'react-i18next';
import {
  HomeOutlined,
  PieChartOutlined,
  FileSearchOutlined,
  LoginOutlined,
  PartitionOutlined,
  PlusOutlined
} from '@ant-design/icons';
import { useSelector } from 'react-redux';
import {
  selectToken,
  logout,
  selectUserInfo,
  selectApiReady
} from '../../store/appSlice';
import { useDispatch } from 'react-redux';
import { useHistory } from "react-router-dom";
import { getOrgProjectsReq } from '../../api/rest/queries/project';
import { getUserInfoReq } from '../../api/rest/queries/user';
import { updateUserInfo } from '../../store/appSlice';

const { Header, Content, Sider } = Layout;
const { SubMenu } = Menu;

export const Navigation: FunctionComponent = () => {
  const [collapsed, setCollapsed] = useState(true);
  const [projects, setProjects] = useState([]);
  const token = useSelector(selectToken);
	const { t } = useTranslation();
	const dispatch = useDispatch();
	const history = useHistory();
  const userInfo = useSelector(selectUserInfo);
  const apiReady = useSelector(selectApiReady);

  const getOrgProjects = () => {
    const id = userInfo.organization || userInfo._id;

    getOrgProjectsReq(id).then(response => {
      setProjects(response.data.docs);
    }).catch(err => {
      message.error(t('nav.fetchProjectsError'));
    })
  }

  useEffect(() => {
    if(!projects.length && apiReady) {
      fetchUserInfo();
      getOrgProjects();
    }
  }, [apiReady])

  const fetchUserInfo = () => {
    getUserInfoReq(userInfo.id).then(response => {
			dispatch(updateUserInfo(response.data));
    })
  }
  
  const handleLogout = () => {
		dispatch(logout())
		history.push('/');
  }

  const handleMenuClick = (values: any) => {
    history.push(values.key);
  }

  return (
    <>
      { token &&  
        <Sider collapsible collapsed={collapsed} onCollapse={() => setCollapsed(!collapsed)}>
          <LogoNav src={logoNav}/>
          <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline" onClick={handleMenuClick}>
            <Menu.Item key="/home" icon={<HomeOutlined />}>
              {t('nav.home')}
            </Menu.Item>
            <SubMenu key="sub1" icon={<PartitionOutlined />} title={t('nav.projects')}>
              { projects.map((project: any) => (
                <Menu.Item key={`/projects/${project._id}`}>{ project.name }</Menu.Item>
              ))}
              <Menu.Item key="/projects/add" icon={<PlusOutlined/>}>{t('nav.add')}</Menu.Item>
            </SubMenu>
            <Menu.Item key="/reports" icon={<FileSearchOutlined />}>
              {t('nav.reports')}
            </Menu.Item>
            <Menu.Item key="/" icon={<LoginOutlined />} onClick={handleLogout}>
              {t('nav.logout')}
            </Menu.Item>
          </Menu>
        </Sider>
      }
      <Layout className="site-layout">
        <Header style={{ padding: 0 }}/>
        <Content style={{ margin: '0 16px' }}>
          <AppRouter />
        </Content>
      </Layout>
    </>
  );
}