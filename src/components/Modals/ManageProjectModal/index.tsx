import React, { useEffect, useState } from 'react';
import { Modal, Button, message, Input, Form } from 'antd';
import { useTranslation } from 'react-i18next';
import { updateProjectReq } from '../../../api/rest/queries/project';
import { ManageProjectModalProps } from '../../../interfaces/components'
const { TextArea } = Input;


export const ManageProjectModal = ({ isVisible, handleOk, handleCancel, project }: ManageProjectModalProps) => {
	const { t } = useTranslation();
  const [loading, setLoading] = useState<boolean>(false);
  const [ form ] = Form.useForm()

  useEffect(() => {
    form.setFieldsValue(project);
  }, [project])

  const validateMessages = {
    required: '${label} é obrigatório!'
  };

  const handleSaveProject = (values: any) => {
    if(values.name !== project.name || values.description !== project.description) {
      setLoading(true);
      updateProjectReq(project._id, values).then(response => {
        message.success(t('projects.updateProjectSuccess'))
        handleOk(response.data);
      }).catch(err => {
        message.error(t('projects.updateProjectError'))
      }).finally(() => {
        setLoading(false);
      })
    }
  }

  return (
    <Modal title={t('projects.modalTitle')} visible={isVisible} footer={false} onCancel={handleCancel}>
      <Form form={form} name="task-form" onFinish={handleSaveProject} validateMessages={validateMessages}>

        <Form.Item name="name" label={t('manageTask.form.name')} rules={[{ required: true }]}>
          <Input />
        </Form.Item>

        <Form.Item name="description" label={t('manageTask.form.description')}>
          <TextArea />
        </Form.Item>

        <Form.Item style={{textAlign: 'end'}}>
          <Button htmlType="button" onClick={handleCancel} disabled={loading}>
            { t('button.cancel') }
          </Button>
          <Button type="primary" htmlType="submit" style={{marginLeft: '10px'}} disabled={loading}>
            { t('button.send') }
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
}
