import React, { useEffect, useState } from 'react';
import { Modal, Button, message, Input, Form, Avatar, InputNumber, Select } from 'antd';
import { useTranslation } from 'react-i18next';
import { getUserInfoReq } from '../../../api/rest/queries/user';
import { createTaskReq, updateTaskReq, removeUserFromTaskReq, addUserToTaskReq, updateTaskSprintReq } from '../../../api/rest/queries/task';
import { useParams } from "react-router-dom";
import { ManageTaskModalProps } from '../../../interfaces/components'
import { UserSearchInput } from '../../../atoms/UserSearchInput'
import { MinusCircleOutlined, UserOutlined } from '@ant-design/icons';
import { TextWrapper } from './style';
const { TextArea } = Input;
const { Option } = Select;


export const ManageTaskModal = ({ isVisible, handleOk, handleCancel, task, project }: ManageTaskModalProps) => {
	const { t } = useTranslation();
  const { id } = useParams<{ id: string }>();
  const [selectedUser, setSelectedUser] = useState<any>('')
  const [loading, setLoading] = useState<boolean>(false);
  const [ form ] = Form.useForm()

  const validateMessages = {
    required: '${label} é obrigatório!'
  };

  useEffect(() => {
    form.setFieldsValue(task);
    if(task && task.user) {
      setSelectedUser(task.user)
    }
    if(task && task.sprint) {
      form.setFieldsValue({sprint: task.sprint._id})
    }
  }, [task])

  const handleSaveTask = (values: any) => {
    if(task) {
      if(values.sprint === 'no-sprint') values.sprint = null
      handleEditTask(values);
    } else {
      handleCreateTask(values);
    }
  }

  const handleCreateTask = (values: any) => {
    const reqObj = {
      task: values,
      project: id
    }

    setLoading(true);
    createTaskReq(reqObj).then(response => {
      message.success(t('manageTask.createTaskSuccess'));
      setSelectedUser('');
      form.resetFields();
      handleOk()
    }).catch(err => {
      message.error(t('manageTask.createTaskError'))
    }).finally(() => {
      setLoading(false);
    })
  }

  const handleEditTask = (values: any) => {
    setLoading(true);
    updateTaskReq(task._id, values).then(response => {
      message.success(t('manageTask.updateTaskSuccess'));
      setSelectedUser('');
      form.resetFields();
      handleOk()
    }).catch(err => {
      message.error(t('manageTask.updateTaskError'))
    }).finally(() => {
      setLoading(false);
    })
  }

  const handleSelect = (user: any) => {
    if(task) {
      addUserToTaskReq(task._id, {user}).then(response => {
        setSelectedUser(response.data.user)
        message.success(t('manageTask.addUserSuccess'))
      }).catch(err => {
        message.error(t('manageTask.addUserError'))
      })
    } else {
      getUserInfoReq(user).then(response => {
        setSelectedUser(response.data)
        form.setFieldsValue({user: response.data.id})
      }).catch(err => {
        message.error(t('general.error'))
      })
    }
  }

  const handleRemoveUser = () => {
    if(task) {
      removeUserFromTaskReq(task._id, {user: selectedUser.id}).then(response => {
        setSelectedUser('')
        form.setFieldsValue({user: null})
        message.success(t('manageTask.removeUserSuccess'));
      })
    } else {
      setSelectedUser('')
      form.setFieldsValue({user: null})
      message.error(t('manageTask.removeUserError'));
    }
  }

  const handleSelectSprint = (value: any) => {
    updateTaskSprintReq(task._id, {sprint: value}).then(response => {
      message.success(t('manageTask.updateSprintSuccess'));
    }).catch(err => {   
      message.error(t('manageTask.updateSprintError'));
    })  
  }

  return (
    <Modal title={t('manageTask.modalTitle')} visible={isVisible} footer={false} onCancel={handleCancel}>
      <Form form={form} name="task-form" onFinish={handleSaveTask} validateMessages={validateMessages}>

        <Form.Item name="name" label={t('manageTask.form.name')} rules={[{ required: true }]}>
          <Input />
        </Form.Item>

        <Form.Item name="description" label={t('manageTask.form.description')}>
          <TextArea />
        </Form.Item>

        <Form.Item name="sprint" label={t('manageTask.form.sprint')}>
          <Select placeholder={t('board.sprintSelect')} onChange={handleSelectSprint} disabled={loading}>
            <Option value="no-sprint">{t('manageTask.removeSprint')}</Option>
            { project && project.sprints && project.sprints.map((item: any) => (
              <Option value={item._id}>{item.name}</Option>
            ))}
          </Select>
        </Form.Item>

        <Form.Item name="user" label={t('manageTask.form.user')}>
          { !selectedUser && <UserSearchInput handleSelectUser={handleSelect}/> }
          { selectedUser && 
            <span className="user">
              <Avatar icon={<UserOutlined />} />
              <TextWrapper>{selectedUser.fullName}</TextWrapper>
              <MinusCircleOutlined onClick={() => handleRemoveUser()} />
            </span> }
        </Form.Item>

        <Form.Item 
          name="estimated" 
          label={t('manageTask.form.estimated')} 
          tooltip={t('manageTask.form.estimatedTooltip')} 
          rules={[{ required: true }]} 
          initialValue={1}
        >
          <InputNumber min={1} max={99} />
        </Form.Item>

        <Form.Item
          name="type"
          label={t('manageTask.form.type')}
          rules={[{ required: true }]}
        >
          <Select placeholder={t('manageTask.form.typePlaceholder')}>
            <Option value="backlog">{t('manageTask.form.typeBacklog')}</Option>
            <Option value="bug">{t('manageTask.form.typeBug')}</Option>
            <Option value="refactor">{t('manageTask.form.typeRefactor')}</Option>
          </Select>
        </Form.Item>
        
        { task && 
          <Form.Item
            name="status"
            label={t('manageTask.form.status')}
            rules={[{ required: true }]}
          >
            <Select placeholder={t('manageTask.form.statusPlaceholder')}>
              <Option value="open">{t('board.open')}</Option>
              <Option value="doing">{t('board.doing')}</Option>
              <Option value="testing">{t('board.testing')}</Option>
              <Option value="done">{t('board.done')}</Option>
            </Select>
          </Form.Item>
        }

        <Form.Item style={{textAlign: 'end'}}>
          <Button htmlType="button" onClick={handleCancel} disabled={loading}>
            { t('button.cancel') }
          </Button>
          <Button type="primary" htmlType="submit" style={{marginLeft: '10px'}} disabled={loading}>
            { t('button.send') }
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
}
