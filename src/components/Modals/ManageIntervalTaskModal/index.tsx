import React, { useEffect, useState } from 'react';
import { Modal, message, Input, AutoComplete, Typography,Row, Tag, Divider, Space, Button, TimePicker } from 'antd';
import { useTranslation } from 'react-i18next';
import { addUserToProjectReq, removeUserFromProjectReq } from '../../../api/rest/queries/project';
import { getUsersByNameReq } from '../../../api/rest/queries/user';
import { useParams } from "react-router-dom";
import { IntervalTaskModalProps } from '../../../interfaces/components';
import { UserList } from '../../../atoms/UserList';
import { TaskSearchInput } from '../../../atoms/TaskSearchInput'
import moment from 'moment';

const { Search } = Input
const { Title } = Typography

export const ManageIntervalTaskModal = ({ isVisible, handleOk, handleCancel, interval }: IntervalTaskModalProps) => {
	const { t } = useTranslation();
  const { id } = useParams<{ id: string }>();
  const [infoInterval, setInfoInterval] = useState(interval)

  useEffect(() => {
    if(interval) {
      setInfoInterval(interval)
    }
  }, [interval])

  const handleAddInterval = () => {
    const newInfoInterval = {...infoInterval};
    const newInterval = {clockIn: moment().format(), clockOut: moment().format()}
    newInfoInterval.intervals ? newInfoInterval.intervals.push(newInterval) : newInfoInterval.intervals = [newInterval]
    setInfoInterval(newInfoInterval)
  }

  const handleSelectTime = (time: any, index: any, type: string) => {
    const newIntervals = {...infoInterval}
    newIntervals.intervals[index][type] = time
    setInfoInterval(newIntervals)
  }

  const handleLinkTask = (task: any, index: any) => {
    const newIntervalsLink = {...infoInterval}
    newIntervalsLink.intervals[index].task = task
    setInfoInterval(newIntervalsLink)
  }

  return (
    <Modal width={650} title={`${t('intervalTask.modalTitle')} ${infoInterval.date}`} visible={isVisible} onOk={() => handleOk(infoInterval)} onCancel={handleCancel}>
      { infoInterval && 
        <>
          <Title level={5}>{ t('intervalTask.intervals') }</Title>
          { infoInterval.intervals && infoInterval.intervals.map((item: any, index: any) => (
            <>        
              <Row key={index}>
                <TimePicker value={moment(item.clockIn)} format={'HH:mm'} onSelect={(time) => handleSelectTime(time, index, 'clockIn')} style={{marginRight: '5px'}}/>
                <TimePicker value={item.clockOut ? moment(item.clockOut) : null} format={'HH:mm'} onSelect={(time) => handleSelectTime(time, index, 'clockOut')} style={{marginRight: '10px'}}/> 
                <TaskSearchInput searchTermOut={item.task ? item.task.name : ''} handleSelectTask={(task) => handleLinkTask(task, index)}/>
              </Row>
              <Divider></Divider>
            </>
            
          ))}
          <Button onClick={handleAddInterval}>{t('intervalTask.addInterval')}</Button>
        </>
      }
    </Modal>
  );
}
