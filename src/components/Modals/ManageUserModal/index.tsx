import React, { useEffect, useState } from 'react';
import { Modal, message, Input, AutoComplete} from 'antd';
import { useTranslation } from 'react-i18next';
import { addUserToProjectReq, removeUserFromProjectReq } from '../../../api/rest/queries/project';
import { getUsersByNameReq } from '../../../api/rest/queries/user';
import { useParams } from "react-router-dom";
import { ManageUserModalProps } from '../../../interfaces/components';
import { UserList } from '../../../atoms/UserList';
import { UserSearchInput } from '../../../atoms/UserSearchInput'

const { Search } = Input

export const ManageUserModal = ({ isVisible, handleOk, handleCancel, users }: ManageUserModalProps) => {
	const { t } = useTranslation();
  const { id } = useParams<{ id: string }>();
  const [userArray, setUserArray] = useState<any>(users || []);
  const [loading, setLoading] = useState<boolean>(false);
  const [updatedProject, setUpatedProject] = useState<any>({})

  useEffect(() => {
    if(users) {
      setUserArray(users)
    }
  }, [users])

  const handleSelectUser = (item: any) => {
    setLoading(true);

    addUserToProjectReq(id, { userId: item }).then(response => {

      if(response.data.users.length === userArray.length) {
        return message.warning(t('manageUser.userAlreadyIn'))
      }

      setUserArray(response.data.users);
      setUpatedProject(response.data);

      message.success(t('manageUser.addSuccess'));
    }).catch(err => {
      message.error(t('manageUser.addError'));
    }).finally(() => {
      setLoading(false);
    })
  }

  const handleRemoveUser = (item: any) => {
    setLoading(true);

    removeUserFromProjectReq(id, {userId: item._id}).then(response => {

      setUserArray(response.data.users);
      setUpatedProject(response.data);
      
      message.success(t('manageUser.removeSuccess'));
    }).catch(err => {
      message.error(t('manageUser.removeError'));
    }).finally(() => {
      setLoading(false)
    })
  }

  return (
    <Modal title={t('manageUser.modalTitle')} visible={isVisible} onOk={() => handleOk(updatedProject)} onCancel={handleCancel}>
      <UserSearchInput handleSelectUser={handleSelectUser} loading={loading}/>
      <UserList users={userArray} handleRemove={handleRemoveUser} loading={loading}/>
    </Modal>
  );
}
