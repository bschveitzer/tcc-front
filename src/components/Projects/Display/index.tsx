import React, { FunctionComponent, useEffect, useState } from 'react';
import { PageWrapper } from '../../../style/global';
import { Card, Typography, message, Button, PageHeader, Divider, Row } from 'antd';
import { useTranslation } from 'react-i18next';
import { getProjectReq } from '../../../api/rest/queries/project';
import { useParams, useHistory } from "react-router-dom";
import { useSelector } from 'react-redux';
import { selectApiReady } from '../../../store/appSlice';
import { ManageUserModal } from '../../Modals/ManageUserModal';
import { ManageTaskModal } from '../../Modals/ManageTaskModal';
import { ManageProjectModal } from '../../Modals/ManageProjectModal'
import { UserList } from '../../../atoms/UserList'
import { TaskList } from '../../../atoms/TaskList'

const { Title, Paragraph } = Typography;

export const DisplayProject: FunctionComponent = () => {
	const { t } = useTranslation();
  const { id } = useParams<{ id: string }>();
  const apiReady = useSelector(selectApiReady);
  const history = useHistory();
  const [project, setProject] = useState<any>({});
  const [loading, setLoading] = useState<boolean>(false);
  const [manageUserModal, setManageUserModal] = useState<boolean>(false);
  const [manageTaskModal, setManageTaskModal] = useState<boolean>(false);
  const [manageProjectModal, setManageProjectModal] = useState<boolean>(false);

  useEffect(() => {
    if(project._id !== id && apiReady && !loading) {
      getProjectInfo();
    } 
  }, [id])

  const getProjectInfo = () => {
    setLoading(true);
    getProjectReq(id).then(response => {
      setProject(response.data);
    }).catch(err => {
      console.error(err);
      message.error(t('projects.errorFetch'));
    }).finally(() => {
      setLoading(false);
    })
  }

  const handleManageUserOk = (projectUpdated: any) => {
    setProject(projectUpdated);
    setManageUserModal(false);
  }

  const handleManageTaskOk = () => {
    setManageTaskModal(false);
    getProjectInfo();
  }

  const handleManageProjectOk = (upatedProject: any) => {
    setManageProjectModal(false);
    setProject(upatedProject);
  }

  return (
    <PageWrapper>
      <PageHeader
        ghost={false}
        onBack={() => history.push('/projects')}
        title={ project.name }
        extra={[
          <Button key="board" onClick={() => history.push(`/board/${project._id}`)}>{ t('projects.board') }</Button>,
          <Button key="edit" type="primary" onClick={() => setManageProjectModal(true)}>{ t('projects.edit') }</Button>,
        ]}
      />
      <Card>
        { project.description && <>
          <Paragraph>{ project.description }</Paragraph>
          <Divider/>
        </>}
        <Title level={5}>{ `${t('projects.creationDate')} ${project.createdAt}` }</Title>
        <Divider/>
        <Row>
          <Title style={{paddingRight: '10px'}} level={4}>{ t('projects.users') }</Title>
          <Button onClick={() => setManageUserModal(true)}>{ t('button.manage') }</Button>
        </Row>
        <UserList users={project.users} />
        <Divider/>
        <Row>
          <Title style={{paddingRight: '10px'}} level={4}>{ t('projects.tasks') }</Title>
          <Button onClick={() => setManageTaskModal(true)}>{ t('button.add') }</Button>
        </Row>

        <TaskList tasks={project.tasks}/>
      </Card>
      <ManageUserModal users={project.users} isVisible={manageUserModal} handleOk={handleManageUserOk} handleCancel={() => setManageUserModal(false)}/>
      <ManageTaskModal isVisible={manageTaskModal} handleOk={handleManageTaskOk} handleCancel={() => setManageTaskModal(false)} project={project}/>
      <ManageProjectModal project={project} isVisible={manageProjectModal} handleOk={handleManageProjectOk} handleCancel={() => setManageProjectModal(false)}/>
    </PageWrapper>
  );
}
