import React, { FunctionComponent, useState } from 'react';
import { PageWrapper } from '../../../style/global';
import { Form, Input, Button, Card, Typography, InputNumber, message } from 'antd';
import { useTranslation } from 'react-i18next';
import { createProjectReq } from '../../../api/rest/queries/project';
import { useHistory } from "react-router-dom";
import { useSelector } from 'react-redux';
import { selectUserInfo } from '../../../store/appSlice';

const { Title } = Typography;

export const FormProject: FunctionComponent = () => {
  const [loading, setLoading] = useState(false);
	const { t } = useTranslation();
	const history = useHistory();
  const userInfo = useSelector(selectUserInfo)

  const [projectForm] = Form.useForm();

  const onFinish = (values: any) => {
		if(values.name) {
      setLoading(true);
      
      values.organization = userInfo.organization || userInfo._id;

			createProjectReq(values).then(response => {
        message.success(t('projects.success'))
        history.push(`/projects/${response.data._id}`);
        projectForm.resetFields();
			}).catch(err => {
        if(err.response.status === 400) {
          message.error(t('projects.error400'))
        } else {
          message.error(t('general.error404'))
        }
			}).finally(() => {
        setLoading(false);
      })
		}
	};

  return (
    <PageWrapper>
      <Title>{ t('projects.add')}</Title>
      <Card>
        <Form
          name="projectForm"
          onFinish={onFinish}
          form={projectForm}>
          <Form.Item
            label={ t('projects.name')}
            name="name"
            rules={[{ required: true, message:  t('projects.required')}]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label={ t('projects.description')}
            name="description"
          >
            <Input />
          </Form.Item>

          <Form.Item
            label={ t('projects.estimated')}
            name="estimated"
          >
            <InputNumber 
              min={1} 
              max={999}/>
              <span className="ant-form-text"> { t('projects.days')}</span>
          </Form.Item>

          <Form.Item style={{ textAlign: 'end' }}>
            <Button type="primary" htmlType="submit" loading={loading}>
            { t('projects.cta')}
            </Button>
          </Form.Item>
        </Form>
			</Card>
    </PageWrapper>
  );
}
