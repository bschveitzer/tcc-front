import styled from 'styled-components';

export const TextWrapper = styled.span`
  margin: 0 5px 0 5px;
`;