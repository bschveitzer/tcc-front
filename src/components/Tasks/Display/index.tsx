import React, { FunctionComponent, useEffect, useState } from 'react';
import { PageWrapper } from '../../../style/global';
import { Card, Typography, message, Button, PageHeader, Divider, Row, Avatar, Tag } from 'antd';
import { useTranslation } from 'react-i18next';
import { getTaskReq } from '../../../api/rest/queries/task';
import { useParams, useHistory } from "react-router-dom";
import { useSelector } from 'react-redux';
import { selectApiReady } from '../../../store/appSlice';
import { ManageTaskModal } from '../../Modals/ManageTaskModal';
import { MinusCircleOutlined, UserOutlined } from '@ant-design/icons';
import { TextWrapper } from './style'
import { getTagColor, getStatusTagColor } from '../../../assets/utils/factory'

const { Title, Paragraph } = Typography;

export const DisplayTask: FunctionComponent = () => {
	const { t } = useTranslation();
  const { id } = useParams<{ id: string }>();
  const apiReady = useSelector(selectApiReady);
  const history = useHistory();
  const [task, setTask] = useState<any>({});
  const [project, setProject] = useState<any>({});
  const [loading, setLoading] = useState<boolean>(false);
  const [manageTaskModal, setManageTaskModal] = useState<boolean>(false);

  useEffect(() => {
    if(task._id !== id && apiReady && !loading) {
      getTaskInfo();
    } 
  }, [id])

  const getTaskInfo = () => {
    setLoading(true);
    getTaskReq(id).then(response => {
      setTask(response.data.task);
      setProject(response.data.project);
    }).catch(err => {
      console.error(err);
      message.error(t('projects.errorFetch'));
    }).finally(() => {
      setLoading(false);
    })
  }

  const handleManageTaskOk = () => {
    setManageTaskModal(false);
    getTaskInfo();
  }

  return (
    <PageWrapper>
      <PageHeader
        ghost={false}
        onBack={() => history.goBack()}
        title={ [`#${task.number} - ${task.name}  `, 
          <Tag color={getTagColor(task.type)}>{task.type && task.type.toUpperCase()}</Tag>, 
          <Tag color={getStatusTagColor(task.status)}>{task.status && task.status.toUpperCase()}</Tag>
        ]}
        extra={[
          <Button key="edit" type="primary" onClick={() => setManageTaskModal(true)}>{ t('projects.edit') }</Button>,
        ]}
      />
      <Card>
        { task.description && <>
          <Paragraph>{ task.description }</Paragraph>
          <Divider/>
        </>}
        <Title level={5}>{ `${t('task.sprint')}` } {task.sprint && `${task.sprint.name}` || t('task.noSprint') }</Title>
        <Title level={5}>{ `${t('task.creationDate')} ${task.createdAt}` }</Title>
        <Title level={5}> {t('task.user')} { task.user && 
            <span className="user">
              <Avatar icon={<UserOutlined />} />
              <TextWrapper>{task.user.fullName}</TextWrapper>
            </span> }
        </Title>
        <Divider/>
        <Title level={5}> {t('task.estimated')} { task.estimated } { t('task.days') }</Title>
        <Title level={5}> {t('task.completed')} { task.completed || '0' } { t('task.days') }</Title>
        <Divider/>
      </Card>
      <ManageTaskModal task={task} isVisible={manageTaskModal} handleOk={handleManageTaskOk} handleCancel={() => setManageTaskModal(false)} project={project}/>
    </PageWrapper>
  );
}
