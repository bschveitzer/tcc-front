export const pt = {
  translation: {
    button: {
      send: 'Enviar',
      add: 'Adicionar',
      remove: 'Remover',
      manage: 'Gerenciar',
      cancel: 'Cancelar'
    },
    form: {
      userName: 'Usuário',
      password: 'Senha',
      reqLogin: 'Insira seu e-mail.',
      reqPassword: 'Insira sua senha.',
      invalidCredentials: 'Credenciais inválidas.'
    },
    footer: {
      message: 'Scout App ©2021 Created by Scout Inc'
    },
    nav: {
      home: 'Início',
      projects: 'Projetos',
      analytics: 'Gerenciamento',
      reports: 'Relatórios',
      logout: 'Sair',
      add: 'Adicionar',
      fetchProjectsError: 'Erro ao buscar projetos.'
    },
    home: {
      welcome: 'Bem vindo! Hoje é ',
      tasks: 'Essas são suas tarefas:',
      hours: 'Seus últimos lançamentos:',
      newDay: 'Inicie seu dia:',
      clockIn: 'Começar',
      clockOut: 'Pausar'
    },
    projects: {
      welcome: 'Projetos',
      add: 'Adicionar projeto',
      name: 'Nome:',
      description: 'Descrição:',
      estimated: 'Tempo estimado:',
      required: 'Por favor, insira um nome.',
      days: 'dias',
      success: 'Projeto criado com sucesso!',
      error400: 'Nome de projeto já utilizado!',
      cta: 'Adicionar',
      successFetch: 'Informações do projeto carregadas.',
      errorFetch: 'Erro ao buscar informações do projeto',
      edit: 'Editar',
      creationDate: 'Data de criação: ',
      users: 'Usuários do projeto',
      tasks: 'Tasks',
      modalTitle: 'Gerenciar projeto',
      updateProjectSuccess: 'Projeto atualizado com sucesso!',
      updateProjectError: 'Erro ao atualizar o projeto!',
      board: 'Quadro de acompanhamento'
    },
    general: {
      error404: 'Erro interno do servidor, tente novamente mais tarde!',
      filters: 'Filtros:'
    },
    manageUser: {
      modalTitle: 'Gerenciar usuários',
      searchPlaceholder: 'Adicione usuários',
      notFound: 'Sem resultados...',
      addSuccess: 'Usuário adicionado com sucesso!',
      addError: 'Erro ao adicionar usuário!',
      userAlreadyIn: 'Usuário já está no projeto!',
      removeSuccess: 'Usuário removido com sucesso!',
      removeError: 'Erro ao remover usuário!',
    },
    manageTask: {
      modalTitle: 'Gerenciar task',
      form: {
        name: 'Nome',
        description: 'Descrição',
        user: 'Usuário responsável',
        subtasks: 'Sub-tasks',
        estimated: 'Tempo estimado',
        estimatedTooltip: 'Quantas horas necessárias para completar a task?',
        type: 'Tipo da task',
        typePlaceholder: 'Selecione o tipo da task',
        typeBug: 'Bug',
        typeBacklog: 'Backlog',
        typeRefactor: 'Refatoração',
        accptanceCriteria: 'Critérios de aceite',
        sprint: 'Sprint',
        status: 'Status da task',
        statusPlaceholder: 'Selecione o status da task'
      },
      removeSprint: 'Sem sprint',
      createTaskSuccess: 'Task criada com sucesso!',
      createTaskError: 'Erro ao criar a task!',
      updateTaskSuccess: 'Task atualizada com sucesso!',
      updateTaskError: 'Erro ao atualizar a task!',
      removeUserSuccess: 'Usuário removido com sucesso!',
      removeUserError: 'Erro ao remover usuário!',
      addUserSuccess: 'Usuário adicionado com sucesso!',
      addUserError: 'Erro ao adicionar usuário!',
      updateSprintSuccess: 'Sprint atualizada com sucesso!',
      updateSprintError: 'Erro ao atualizar a sprint!',
      notFound: 'Sem resultados',
      searchPlaceholder: 'Busque pela task'
    },
    reports: {
      title: 'Relatório de horas',
      dayOrder: 'Dia',
      dayHours: 'Horas contabilizadas',
      intervals: 'Intervalos',
      monthHours: 'Banco de horas mensal:'
    },
    board: {
      title: 'Quadro de acompanhamento',
      open: 'Em aberto',
      doing: 'Em Andamento',
      testing: 'Em teste',
      done: 'Finalizado',
      createNewSprint: 'Criar nova sprint',
      createNewSprintSuccess: 'Nova sprint criada com sucesso!',
      createNewSprintError: 'Erro ao criar nova sprint!',
      backlog: 'Exibir backlog completo',
      sprintSelect: 'Escolha a sprint',
      fetchSprintError: 'Erro ao buscar informações da sprint!'
    },
    intervalTask: {
      modalTitle: 'Informações diárias:',
      intervals: 'Intervalos registrados:',
      addInterval: 'Adicionar novo intervalo',
      linkTask: 'Vincular task'
    },
    task: {
      name: 'Nome:',
      description: 'Descrição:',
      estimated: 'Tempo estimado:',
      completed: 'Tempo completado:',
      required: 'Por favor, insira um nome.',
      days: 'dias',
      success: 'Task criado com sucesso!',
      error400: 'Nome de task já utilizado!',
      cta: 'Adicionar',
      successFetch: 'Informações da task carregadas.',
      errorFetch: 'Erro ao buscar informações da task.',
      edit: 'Editar',
      creationDate: 'Data de criação: ',
      user: 'Usuário responsável:',
      status: 'Status da task:',
      sprint: 'Sprint: ',
      noSprint: 'Sem sprint definida'
    },
  }
}