export const getTagColor = (type: string) => {
  switch (type) {
    case 'bug':
      return 'red'
    case 'refactor':
      return 'orange'
    case 'backlog':
      return 'blue'
    default:
      return 'gray'
  }
}

export const getStatusTagColor = (type: string) => {
  switch (type) {
    case 'open':
      return 'magenta'
    case 'doing':
      return 'blue'
    case 'committed':
      return 'purple'
    case 'testing':
      return 'cyan'
    case 'done':
      return 'green'
    default:
      return 'gray'
  }
}