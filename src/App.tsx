import React, { FunctionComponent,useEffect } from 'react';
import { Layout } from 'antd';
import { Navigation } from './components/Navigation';
import { BrowserRouter as Router } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { fetchUser } from './store/appSlice';
const token = localStorage.getItem('token');

export const App: FunctionComponent = () => {
	const dispatch = useDispatch();

  useEffect(() => {
    if(token) {
      dispatch(fetchUser());
    }
  }, [])

  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Router>  
        <Navigation/> 
      </Router>
    </Layout>
  );
}