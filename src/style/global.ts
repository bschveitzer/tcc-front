import styled from 'styled-components';

export const AppWrapper = styled.section`
  height: 100%;
`;

export const LogoImage = styled.img`
  max-width: 200px;
  margin: auto;
`;

export const LogoNav = styled.img`
  height: 60px;
  margin-left: 10px;
`;

export const PageWrapper = styled.div`
  padding: 30px;
`;

export const LoadingWrapper = styled.div`
  text-align: center;
  margin-top: 30px;
`;