import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { API } from '../api/rest/api.config';
import { fetchUserInfoReq } from '../api/rest/queries/auth'

const token = localStorage.getItem('token');

export const fetchUser = createAsyncThunk('user/fetchUser', async () => {
  API.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  const response = await fetchUserInfoReq();
  return response.data;
})

export const appSlice = createSlice({
  name: 'app',
  initialState: {
    token,
    userInfo: {},
    apiReady: false
  },
  reducers: {
    login: (state, action) => {
      localStorage.setItem('token', action.payload.token);
      state.token = action.payload.token;
      state.userInfo = action.payload.userInfo;
      API.defaults.headers.common['Authorization'] = `Bearer ${action.payload.token}`;
    },
    logout: state => {
      state.token = null;
      localStorage.removeItem('token');
      API.defaults.headers.common['Authorization'] = null;
    },
    updateUserInfo: (state, action) => {
      state.userInfo = action.payload;
    }
  },
  extraReducers: {
    [fetchUser.pending]: (state, action) => {
      state.status = 'loading'
    },
    [fetchUser.fulfilled]: (state, action) => {
      state.token = token;
      state.userInfo = action.payload.userInfo;
      state.apiReady = true;
    },
    [fetchUser.rejected]: (state, action) => {
      state.status = 'failed'
      state.error = action.error.message
    }
  }
})

// Action creators are generated for each case reducer function
export const { login, logout, updateUserInfo } = appSlice.actions

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.counter.value)`
export const selectToken = state => state.app.token;
export const selectUserInfo = state => state.app.userInfo;
export const selectApiReady = state => state.app.apiReady;

export default appSlice.reducer