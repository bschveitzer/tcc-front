import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './App';
import './index.scss';
import 'antd/dist/antd.css';
import './i18n';
import store from './store/index'
import { Provider } from 'react-redux'
import moment from 'moment';
import 'moment/locale/pt-br';
moment.locale('pt-br');

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);