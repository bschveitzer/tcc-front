import { API } from '../api.config'

export const createTaskReq = async (values: any) => {
  return await API.post('/task', values);
}

export const getTaskReq = async (taskId: String) => {
  return await API.get(`/task/${taskId}`);
}

export const updateTaskReq = async (taskId: String, values: any) => {
  return await API.put(`/task/${taskId}`, values);
}

export const addUserToTaskReq = async (taskId: String, values: any) => {
  return await API.put(`/add-user-task/${taskId}`, values);
}

export const removeUserFromTaskReq = async (taskId: String, values: any) => {
  return await API.put(`/remove-user-task/${taskId}`, values);
}

export const updateTaskSprintReq = async (taskId: String, values: any) => {
  return await API.put(`/update-sprint-task/${taskId}`, values);
}

export const getTasksByNameReq = async (name: string) => {
  return await API.get(`/tasks-by-name/${name}`);
}
