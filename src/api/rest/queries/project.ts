import { IProject } from '../../../interfaces/api'
import { API } from '../api.config'

export const createProjectReq = async (body: IProject) => {
  return await API.post('/project', body);
}

export const getProjectReq = async (projectId: String) => {
  return await API.get(`/project/${projectId}`);
}

export const getOrgProjectsReq = async (orgId: String) => {
  return await API.get(`/projects/${orgId}`);
}

export const updateProjectReq = async (projectId: String, body: any) => {
  return await API.put(`/project/${projectId}`, body);
}

export const addUserToProjectReq = async (projectId: String, body: any) => {
  return await API.put(`/add-user-project/${projectId}`, body);
}

export const removeUserFromProjectReq = async (projectId: String, body: any) => {
  return await API.put(`/remove-user-project/${projectId}`, body);
}

export const createNewSprintReq = async (projectId: String) => {
  return await API.post(`/create-new-sprint/${projectId}`);
}

export const filterTasksBySprintReq = async (projectId: String, sprintId: String) => {
  return await API.get(`/filter-task-by-sprint/${projectId}&${sprintId}`);
}