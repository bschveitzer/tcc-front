import { API } from '../api.config'

export const getUsersByNameReq = async (name: string) => {
  return await API.get(`/users/${name}`);
}

export const getUserInfoReq = async (id: string) => {
  return await API.get(`/user-by-id/${id}`);
}