import { API } from '../api.config'

export const updateIntervalsReq = async (values: any) => {
  return await API.post('/intervals', values);
}

export const getUserMonthlyReportReq = async (userId: any, month: number, year: number) => {
  return await API.get(`/report/${userId}&${month}&${year}`);
}