import { ILogin } from '../../../interfaces/api'
import { API } from '../api.config'

export const loginReq = async (body: ILogin) => {
  return await API.post('/login', body);
}

export const fetchUserInfoReq = async () => {
  return await API.get('/user/me');
}