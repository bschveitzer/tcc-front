import React, { FunctionComponent, useState, useEffect } from 'react';
import { PageWrapper } from '../../style/global';
import { Typography, Card, Table, Button, Row, Tag, Calendar } from 'antd';
import { useTranslation } from 'react-i18next';
import { ListWrapper } from './style';
import { updateIntervalsReq, getUserMonthlyReportReq } from '../../api/rest/queries/report'
import { ManageIntervalTaskModal } from '../../components/Modals/ManageIntervalTaskModal';
import { useSelector } from 'react-redux';
import {
  selectUserInfo,
} from '../../store/appSlice';
import moment from 'moment';
import 'moment/locale/pt-br'
moment.locale('pt-br');

const { Title } = Typography;

export const Reports: FunctionComponent = () => {
	const { t } = useTranslation();
  const [intervalTaskModal, setIntervalTaskModal] = useState<boolean>(false);
  const [selectedReport, setSelectedReport] = useState<any>({});
  const [monthlyData, setMonthlyData] = useState<any>([])
  const userInfo = useSelector(selectUserInfo);

  useEffect(() => {
    if(userInfo._id) {
      getUserMonthlyReportReq(userInfo._id, moment().month(), moment().year()).then(response => {
        formatData(response.data.intervals)
      })
    }
  }, [userInfo])

  const formatData = (rawData: any) => {
    const sortedData = rawData.sort((a: any, b: any) => {
      return a.dayOrder - b.dayOrder
    })
    
    let monthData = []
    for(let index = 0; index < sortedData.length; index++) {
      if(monthData.length === 0) {
        monthData[0] = {
          dayOrder: sortedData[index].dayOrder,
          intervals: [sortedData[index]]
        }
      } else {
        if(monthData[monthData.length - 1].dayOrder === sortedData[index].dayOrder) {
          monthData[monthData.length - 1].intervals.push(sortedData[index])
        } else {
          monthData.push({
            dayOrder: sortedData[index].dayOrder,
            intervals: [sortedData[index]]
          })
        }
      }
    }

    setMonthlyData(monthData);
  }

  const handleClickDay = (value: any) => {
    let selectedDay = monthlyData.find((item: any) => item.dayOrder === value.date());

    if(!selectedDay) {
      selectedDay = {
        dayOrder: value.date(),
        intervals: []
      }
    }

    selectedDay.date = value.format('L');
    selectedDay.originalDate = value;

    setSelectedReport(selectedDay);
    setIntervalTaskModal(true);
  }

  const handleSaveReport = (report: any) => {
    report.user = userInfo;

    updateIntervalsReq(report).then(response => {
      formatData(response.data.intervals)
      setIntervalTaskModal(false);
    }).catch(err => {

    })
  }

  function dateCellRender(value: any) {
    let listData:any = [];

    for(let index = 0; index < monthlyData.length; index++) {
      if(monthlyData[index].dayOrder === value.date()) {
        listData = monthlyData[index].intervals
        break
      }    
    }
    
    return (
      <ListWrapper>
        {listData.map((item: any) => (
          <li key={item._id}>
            <Tag color={ item.clockIn && item.clockOut ? 'green' : 'red' }>
              {moment(item.clockIn).format('HH:mm')} - {moment(item.clockOut).format('HH:mm')}
            </Tag>
          </li>
        ))}
      </ListWrapper>
    );
  }

  return (
    <PageWrapper>
      <Card>
        <Title level={4}>{ t('reports.title')}</Title>
        <Calendar dateCellRender={dateCellRender} onSelect={handleClickDay}/>,
      </Card>

      <ManageIntervalTaskModal isVisible={intervalTaskModal} handleOk={handleSaveReport} handleCancel={ () => setIntervalTaskModal(false) } interval={selectedReport}/>
    </PageWrapper>
  );
}
