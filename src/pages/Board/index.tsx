import React, { FunctionComponent, useEffect, useState } from 'react';
import { PageWrapper } from '../../style/global';
import { Typography, Card, message, PageHeader, Button, Select, Row } from 'antd';
import { useTranslation } from 'react-i18next';
import { BoardWrapper } from './style';
import { useParams, useHistory } from "react-router-dom";
import { getProjectReq, createNewSprintReq, filterTasksBySprintReq } from '../../api/rest/queries/project';
import { TaskList } from '../../atoms/TaskList';

const { Title } = Typography;
const { Option } = Select;

export const BoardDisplay: FunctionComponent = () => {
	const { t } = useTranslation();
  const { id } = useParams<{ id: string }>();
  const [project, setProject] = useState<any>({});
  const [loading, setLoading] = useState<boolean>(false);
  const [openTasks, setOpenTasks] = useState<any>([]);
  const [doingTasks, setDoingTasks] = useState<any>([]);
  const [testingTasks, setTestingTasks] = useState<any>([]);
  const [doneTasks, setDoneTasks] = useState<any>([]);
  const [selectedSprint, setSelectedSprint] = useState<any>('backlog');
  const history = useHistory();


  useEffect(() => {
    if(project._id !== id) {
      getProjectInfo();
    }
  }, [id])

  const getProjectInfo = () => {
    setLoading(true);
    getProjectReq(id).then(response => {
      setProject(response.data);
      if(!openTasks.length && !doingTasks.length && !testingTasks.length && !doneTasks.length) {
        prepareTasks(response.data.tasks);
      }
    }).catch(err => {
      console.error(err);
    }).finally(() => {
      setLoading(false);
    })
  }

  const prepareTasks = (tasks: any) => {
    for(let index = 0; index < tasks.length; index++) {
      
      if(tasks[index].status === 'open') {
        let newOpenTasks = openTasks;
        newOpenTasks.push(tasks[index])
        setOpenTasks(newOpenTasks)
      } else if(tasks[index].status === 'doing') {
        let newDoingTasks = doingTasks;
        newDoingTasks.push(tasks[index])
        setDoingTasks(newDoingTasks)
      } else if(tasks[index].status === 'testing') {
        let newTestingTasks = testingTasks;
        newTestingTasks.push(tasks[index])
        setTestingTasks(newTestingTasks)
      } else if(tasks[index].status === 'done') {
        let newDoneTasks = doneTasks;
        newDoneTasks.push(tasks[index])
        setDoneTasks(newDoneTasks)
      }
    }
  }

  const clearTasks = () => {
    setOpenTasks([]);
    setDoingTasks([]);
    setTestingTasks([]);
    setDoneTasks([]);
  }

  const handleCreateNewSprint = () => {
    createNewSprintReq(project._id).then(response => {
      setProject(response.data);
      setSelectedSprint(response.data.sprints[response.data.sprints.length - 1]._id);
      clearTasks();
      message.success(t('board.createNewSprintSuccess'))
    }).catch(err => {
      message.error(t('board.createNewSprintError'))
    })
  }

  const handleSelect = (value: any) => {
    clearTasks();
    setSelectedSprint(value);

    if(value === 'backlog') {
      getProjectInfo()
    } else {
      filterTasksBySprintReq(project._id, value).then(response => {
        prepareTasks(response.data.tasks)
      }).catch(err => {
        message.error(t('board.fetchSprintError'))
      })
    }
  }

  return (
    <PageWrapper>
      <PageHeader
        ghost={false}
        onBack={() => history.goBack()}
        title={ `${t('board.title')} - ${project.name}` }
        extra={[
          <Button key="board" onClick={handleCreateNewSprint}>{ t('board.createNewSprint') }</Button>,
        ]}
      />
      <Card>
        <Row>
          <Title level={5} style={{marginRight: '10px'}}>{ t('general.filters')}</Title>
          <Select value={selectedSprint} placeholder={t('board.sprintSelect')} onChange={handleSelect} disabled={loading}>
            <Option value="backlog" selected>{t('board.backlog')}</Option>
            { project.sprints && project.sprints.map((item: any) => (
              <Option value={item._id}>{item.name}</Option>
            ))}
          </Select>

        </Row>
      </Card>
      <BoardWrapper>
        <Card title={t('board.open')} style={{ width: '25%'}}>
          <TaskList tasks={openTasks} />
        </Card>
        <Card title={t('board.doing')} style={{ width: '25%'}}>
          <TaskList tasks={doingTasks} />
        </Card>
        <Card title={t('board.testing')} style={{ width: '25%'}}>
          <TaskList tasks={testingTasks} />
        </Card>
        <Card title={t('board.done')} style={{ width: '25%'}}>
          <TaskList tasks={doneTasks} />
        </Card>
      </BoardWrapper>
    </PageWrapper>
  );
}
