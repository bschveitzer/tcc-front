import React, { FunctionComponent, useState, useEffect } from 'react';
import { PageWrapper } from '../../style/global';
import { Typography, Card, List, Button, Row, Tag } from 'antd';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import { getUserMonthlyReportReq } from '../../api/rest/queries/report'
import { useSelector } from 'react-redux';
import {
  selectUserInfo,
} from '../../store/appSlice';
import { TaskList } from '../../atoms/TaskList'

const { Title } = Typography;

export const Home: FunctionComponent = () => {
  const [todayDate, setTodayDate] = useState(moment().format('LLLL'));
  const userInfo = useSelector(selectUserInfo);
  const [userIntervals, setUserIntervals] = useState()
	const { t } = useTranslation();

  setInterval(() => {
    setTodayDate(moment().format('LLLL'));
  }, 10000)

  useEffect(() => {
    if(userInfo._id) {
      getUserMonthlyReportReq(userInfo._id, moment().month(), moment().year()).then(response => {         
         const sortedData = response.data.intervals.sort((a: any, b: any) => {
          return b.dayOrder - a.dayOrder
        })

        const slicedData = sortedData.slice(0, 4)

        const formattedData = slicedData.map((item: any) => {
          return {...item, date: moment([response.data.year, response.data.monthOrder, item.dayOrder]).format('L')}
        })

        setUserIntervals(formattedData)
      })
    }
  }, [userInfo])

  return (
    <PageWrapper>
      <Title>{ t('home.welcome')}{todayDate}.</Title>
      
      <Card>
        <Title level={4}>{ t('home.hours')}</Title>
        <List
          grid={{ gutter: 16, column: 4 }}
          dataSource={userIntervals}
          renderItem={(item:any) => (
            <List.Item>
              <List.Item.Meta
                title={<span>{moment(item.clockIn).format('HH:mm')} - {moment(item.clockOut).format('HH:mm')}</span>}
                description={item.date}
              />
              {item.task && <span>{item.task.name}</span>}
            </List.Item>
          )}
        />
      </Card>
      <Card>
        <Title level={4}>{ t('home.tasks')}</Title>
        { userInfo && <TaskList tasks={userInfo.tasks}/> }
      </Card>
    </PageWrapper>
  );
}
