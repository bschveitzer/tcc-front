import React, { FunctionComponent, useState } from 'react';
import { LogoImage, PageWrapper } from '../../style/global';
import { Form, Input, Button, Card, Layout } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import logo from '../../assets/brand/logoName.svg';
import { CardWrapper } from './style';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { login } from '../../store/appSlice';
import { loginReq } from '../../api/rest/queries/auth';
import { ILogin } from '../../interfaces/api';
import { ValidateStatus } from 'antd/lib/form/FormItem';
const { Footer } = Layout;

export const Auth: FunctionComponent = () => {
	const { t } = useTranslation();
	const dispatch = useDispatch();
	const [error, setError] = useState<ValidateStatus>();
	const [errorMsg, setErrorMsg] = useState<String>();
		
	const onFinish = (values: ILogin) => {
		if(values.login  && values.password) {
			loginReq(values).then(response => {
				dispatch(login(response.data));
			}).catch(err => {
				setError('error');
				setErrorMsg(t('form.invalidCredentials'));
			})
		}
	};
	
	return (
		<PageWrapper>
			<CardWrapper>
				<Card style={{ width: '300px' }} cover={<LogoImage alt="logo" src={logo} />}>
					<Form
						name="login"
						onFinish={onFinish}
					>
						<Form.Item 
							name="login"
							validateStatus={error}
							rules={[{ required: true, message: t('form.reqLogin') }]}>
							<Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder={ t('form.userName')} />
						</Form.Item>
				
						<Form.Item 
							name="password"
							validateStatus={error}
							help={errorMsg}
							rules={[{ required: true, message: t('form.reqPassword') }]}>
							<Input
								prefix={<LockOutlined className="site-form-item-icon" />}
								type="password"
								placeholder={ t('form.password')}
							/>
						</Form.Item>
				
						<Form.Item style={{ textAlign: 'end' }}>
							<Button type="primary" htmlType="submit">
								{t('button.send')}
							</Button>
						</Form.Item>
					</Form>
				</Card>
			</CardWrapper>			
			<Footer style={{ textAlign: 'center' }}>{ t('footer.message')}</Footer>
		</PageWrapper>
	);
}
