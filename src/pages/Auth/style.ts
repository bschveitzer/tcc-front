import styled from 'styled-components';

export const Wrapper = styled.section`
  padding: 20px;
`;

export const LogoImage = styled.img`
  max-width: 300px;
  margin: auto;
`;

export const CardWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;

export const LoadingWrapper = styled.div`
  text-align: center;
  margin-top: 30px;
`;