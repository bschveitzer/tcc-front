import React, { FunctionComponent, useState } from 'react';
import { PageWrapper } from '../../style/global';
import { Typography } from 'antd';
import moment from 'moment';
import { useTranslation } from 'react-i18next';

const { Title } = Typography;

export const Projects: FunctionComponent = () => {
  const [todayDate, setTodayDate] = useState(moment().format('LLLL'));
	const { t } = useTranslation();

  setInterval(() => {
    setTodayDate(moment().format('LLLL'));
  }, 10000) 

  return (
    <PageWrapper>
      <Title>{ t('home.welcome')}{todayDate}.</Title>
      <Title level={3}>{ t('home.tasks')}</Title>
    </PageWrapper>
  );
}
