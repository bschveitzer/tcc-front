export interface ManageUserModalProps {
  isVisible: boolean;
  handleOk: (project: any) => void;
  handleCancel: () => void;
  users: [];
}

export interface ManageTaskModalProps {
  isVisible: boolean;
  handleOk: () => void;
  handleCancel: () => void;
  task?: any,
  project: any
}

export interface ManageProjectModalProps {
  isVisible: boolean;
  handleOk: (args: any) => void;
  handleCancel: () => void;
  project?: any
}

export interface IntervalTaskModalProps {
  isVisible: boolean;
  handleOk: (report: any) => void;
  handleCancel: () => void;
  interval?: any
}