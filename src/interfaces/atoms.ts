export interface UserListProps {
  handleRemove?: (item: any) => void;
  users: [];
  loading?: boolean;
}

export interface TaskListProps {
  handleRemove?: (item: any) => void;
  tasks: [];
  loading?: boolean;
}

export interface IUserSearchInput {
  handleRemove?: (item: any) => void;
  handleSelectUser: (item: any) => void;
  loading?: boolean;
}

export interface ITaskSearchInput {
  handleRemove?: (item: any) => void;
  handleSelectTask: (item: any) => void;
  loading?: boolean;
  searchTermOut?: string
}