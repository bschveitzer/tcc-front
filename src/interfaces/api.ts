export interface ILogin {
  login: String,
  password: String
}

export interface IProject {
  name: String,
  description?: String,
  estimated?: Number
}

export interface IResponse {
  error?: String,
  data?: String
}