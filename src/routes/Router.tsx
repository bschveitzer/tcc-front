import React, { FunctionComponent } from 'react';
import {
  Switch,
  Route,
  Redirect
} from 'react-router-dom';
import { Home } from '../pages/Home';
import { Auth } from '../pages/Auth';
import { Projects } from '../pages/Projects';
import { Reports } from '../pages/Reports';
import { FormProject } from '../components/Projects/Form';
import { DisplayProject } from '../components/Projects/Display'
import { DisplayTask } from '../components/Tasks/Display'
import { BoardDisplay } from '../pages/Board'

import { useSelector } from 'react-redux';
import {
  selectToken,
} from '../store/appSlice';

export const AppRouter: FunctionComponent = () => {
  const token = useSelector(selectToken);

  const PrivateRoute = ({ component: Component, ...rest }: any) => (
    <Route
      {...rest}
      render={ props => token ? ( <Component {...props} />) : (<Redirect to='/' />) }
    />
  );
  

  return (
    <Switch>
      <Route exact path="/">
        { !token ? <Auth /> : <Redirect to="/home" />}
      </Route>
      <PrivateRoute exact path="/home" component={Home}/>
      <PrivateRoute exact path="/projects" component={Projects}/>
      <PrivateRoute path="/projects/add" component={FormProject}/>
      <PrivateRoute exact path="/projects/:id" component={DisplayProject}/>
      <PrivateRoute exact path="/reports" component={Reports}/>
      <PrivateRoute exact path="/tasks/:id" component={DisplayTask}/>
      <PrivateRoute exact path="/board/:id" component={BoardDisplay}/>
    </Switch>
  );
}