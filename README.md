# TCC FRONT

## Considerações

## Scripts

### `yarn`

Para instalar todas as dependências, necessário!

### `yarn lint`
Verificar os erros de escrita de código.

### `yarn start`

Para rodar a aplicação em si, vai inicializar um servidor de desenvolvimento no [http://localhost:3000](http://localhost:3000).

### `yarn test`

Para rodar todos os casos de teste da aplicação.